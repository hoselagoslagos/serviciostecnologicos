document.addEventListener('DOMContentLoaded', function () {
    let acordeonItems = document.querySelectorAll('.accordion-item');

    acordeonItems.forEach(item => {
        item.addEventListener('show.bs.collapse', function () {
            // Obtener la prioridad del acordeón actual
            let prioridad = item.getAttribute('data-prioridad');
            // Obtener el contenido del ticket correspondiente a la prioridad
            let contenidoTicket = getContenidoTicket(prioridad);
            // Obtener el div para mostrar el contenido del ticket
            let ticketContent = item.querySelector('.ticket-content');
            // Mostrar el contenido del ticket y la prioridad
            ticketContent.innerHTML = contenidoTicket;
        });
    });
});

// Función para obtener el contenido del ticket según la prioridad
function getContenidoTicket(prioridad) {
    let contenido = `
        <h3>Detalles del Ticket</h3>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Problema de red</h5>
                <p class="card-text">Descripción detallada del problema...</p>
                <p class="card-text"><strong>Prioridad:</strong> ${prioridad}</p>
                <p class="card-text"><strong>Estado:</strong> Pendiente</p>
                <p class="card-text"><strong>Asignado a:</strong> John Doe</p>
                <!-- Agrega más detalles según sea necesario -->
            </div>
        </div>
    `;
    return contenido;
}
