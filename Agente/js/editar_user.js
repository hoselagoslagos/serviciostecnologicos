document.addEventListener('DOMContentLoaded', function () {
    // Función para cargar los datos del usuario en el formulario de edición
    function cargarDatosUsuario(nombre, apellido, email, telefono, departamento) {
        document.getElementById('nombre').value = nombre;
        document.getElementById('apellido').value = apellido;
        document.getElementById('email').value = email;
        document.getElementById('telefono').value = telefono;
        document.getElementById('departamento').value = departamento;
    }

    // Función para guardar los cambios del usuario y ocultar el modal después de guardarlos
    function guardarCambios() {
        // Obtener los datos actualizados del formulario
        let nombre = document.getElementById('nombre').value;
        let apellido = document.getElementById('apellido').value;
        let email = document.getElementById('email').value;
        let telefono = document.getElementById('telefono').value;
        let departamento = document.getElementById('departamento').value;

        // Mostrar los datos actualizados en la consola (esto es opcional, puedes eliminarlo si lo deseas)
        document.querySelector('#name').innerText = nombre;
        document.querySelector('#lastname').innerText = apellido;
        document.querySelector('#correo').innerText = email;
        document.querySelector('#phone').innerText = telefono;
        document.querySelector('#departament').innerText = departamento;
        console.log(nombre);
        console.log(apellido);
        console.log(email);
        console.log(telefono);
        console.log(departamento);

        // Cerrar el modal después de guardar los cambios
        let modal = document.getElementById('editarUsuarioModal');
        let modalBootstrap = bootstrap.Modal.getInstance(modal);
        modalBootstrap.hide();
    }

    // Asignar evento al botón "Guardar Cambios" para mostrar los datos actualizados en la consola y ocultar el modal
    document.getElementById('btnGuardarCambios').addEventListener('click', function (event) {
        event.preventDefault(); // Evitar que se recargue la página al hacer clic en el botón
        guardarCambios();
    });

    cargarDatosUsuario('Mark', 'Otto', 'mo@mdo.com', '+569981873771', 'Finanzas');
    
});
