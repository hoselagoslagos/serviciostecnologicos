document.addEventListener('DOMContentLoaded', function () {
    // Obtener todos los botones "Eliminar"
    let botonesEliminar = document.querySelectorAll('.btn-danger');

    // Iterar sobre cada botón "Eliminar" y agregar un evento de clic
    botonesEliminar.forEach(boton => {
        boton.addEventListener('click', function () {
            // Obtener la fila a la que pertenece el botón
            let filaUsuario = this.closest('tr');
            
            // Confirmar la eliminación antes de proceder
            if (confirm('¿Estás seguro de que deseas eliminar este usuario?')) {
                // Eliminar la fila del usuario
                filaUsuario.remove();
            }
        });
    });
});
