document.addEventListener('DOMContentLoaded', function () {
    const formAsignacion = document.getElementById('formAsignacion'); // Obtenemos el formulario
    const tablaAsignacion = document.getElementById('tablaAsignacion'); // Obtenemos el div para la tabla

    formAsignacion.addEventListener('submit', function (event) {
        event.preventDefault(); // Evitamos que el formulario se envíe

        // Obtenemos los valores del formulario
        const ticketId = document.getElementById('ticketId').value;
        const asignadoA = document.getElementById('asignadoA').value;

        // Creamos la tabla con los datos
        const tablaHTML = `
            <h4>Asignación Guardada:</h4>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID del Ticket</th>
                        <th>Asignado a</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${ticketId}</td>
                        <td>${asignadoA}</td>
                    </tr>
                </tbody>
            </table>
        `;

        // Mostramos la tabla en el div correspondiente
        tablaAsignacion.innerHTML = tablaHTML;
    });
});
